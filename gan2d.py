import mido
from mido import MidiFile
from mido import MidiTrack
from mido import MetaMessage
import io
import tensorflow as tf
import tensorflow.contrib.gan as tfgan
import tensorflow.contrib.framework as framework
import os
import pickle
import matplotlib.pyplot as plt
from tensorflow.python.training import monitored_session
import time
from os.path import isfile, join
import math
from six.moves import xrange
import numpy as np

layers = tf.contrib.layers
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'
tf.logging.set_verbosity(tf.logging.ERROR)


slim = tf.contrib.slim
flags = tf.app.flags
FLAGS = flags.FLAGS
flags.DEFINE_float(
    'weight_factor', 50000000.0,
    'How much to weight the adversarial loss relative to note loss.')



def track_to_roll(track):
    meta = []
    notes_on = set()
    b_track = []
    for me in track:
        print(me)
        if me.type != 'note_on' and me.type != 'note_off':
            meta.append(me)
        elif me.bytes()[2] != 0:
            notes_on.add(me.bytes()[1])
        else:
            if me.bytes()[1] in notes_on:
                notes_on.remove(me.bytes()[1])
        raw_time = me.time
        if raw_time != 0:
            row = []

            for i in range(0, 128):
                if i in notes_on:
                    row.append(1)
                else:
                    row.append(0)
            num_rows = int(math.floor(raw_time/3))
            for i in range(num_rows):
               b_track.append(row)
    return b_track


def store(roll_track,prefix='stored_roleld'):
        visualize_piano_roll(roll_track)
        squared = []
        for l in roll_track:
            t = []
            for n in l:
                if n >= 0.003:
                    t.append(float(1))
                else:
                    t.append(float(0))
            squared.append(t)

        visualize_piano_roll(squared)

        mid = MidiFile()
        track = MidiTrack()
        now = 0
        notes_on = set()
        for row in squared:
            track.append(mido.MetaMessage(type='text',time=60))
            for i,v in enumerate(row):
                if v >= 1 and not i in notes_on:
                    message = mido.Message('note_on', note=i, velocity=64, time=0)
                    notes_on.add(i)
                    track.append(message)
                if v<1 and i in notes_on:
                    message = mido.Message('note_on', note=i,velocity =0, time=0)
                    notes_on.remove(i)
                    track.append(message)

        end = MetaMessage('end_of_track')
        track.append(end)
        mid.tracks.append(track)
        mid.save('outputs/roll_'+ str(prefix)+ '_' + str(int(round(time.time() * 1000))) + '.mid')


def store_image(sess, generator_tensor, prefix='none', num_of_samples=3,iteration=0):
    f_notes = []
    for _ in range(num_of_samples):
        ret = sess.run(generator_tensor)
        f_notes = f_notes + list(ret[0])
        # print(str([ int(v*255) for v in images_np]))
    # f_notes=[min([127,round(n*MAX_VAL)]) for n in f_notes ]
    squared = []
    note_counter = 0
    for l in f_notes:
        t = []
        for n in l:
            if n > 0.008:
                t.append(float(1))
                note_counter+=1
            else:
                t.append(float(0))
        squared.append(t)
    print('note_count: ' + str(note_counter))
    if(note_counter > 600):
        store(squared,prefix='_squared_track_wn_' + str(note_counter))
    visualize_piano_roll(squared, prefix='evolve_squared/squared_check' + str(iteration))
    visualize_piano_roll(f_notes,prefix='evolve/generated_check' + str(iteration))

def store_output_and_check_loss(sess, generator_tensor, prefix='none', num_of_samples=5):
    f_notes = []
    for _ in range(num_of_samples):
        ret = sess.run(generator_tensor)
        f_notes = f_notes + list(ret[0])
        # print(str([ int(v*255) for v in images_np]))
    # f_notes=[min([127,round(n*MAX_VAL)]) for n in f_notes ]
    visualize_piano_roll(f_notes)
    store(f_notes, prefix=prefix)


def visualize_piano_roll(images_np,prefix='gen',show=False):
    plt.axis('off')
    plt.imsave(fname='out_rolls/'+str(prefix)+'_'+str(int(round(time.time() * 1000)))+'.png',arr=np.squeeze(images_np), cmap='gray')



mypath = 'MIDI'
SNNIPET_LENGTH = 400
NUM_SAMPLES = 1
NOTE_ON_THRESHOLD = 0.01

tracks = []

log_folder = 'log_n_'+ str(SNNIPET_LENGTH)+'_'+str(int(round(time.time() * 1000)))
#log_folder = 'log_n_256_1526856088130'
print('log folder:' + log_folder)


for f in ['appass_1.mid','sm64_title.mid']:  # listdir(mypath)
    print('scaning: ' + str(f))
    with io.open(join(mypath, f), 'rb') as file:
        track = MidiFile(file=file)
        merged = mido.merge_tracks(track.tracks)
        roll_track = track_to_roll(merged)
        tracks.append(roll_track[0:SNNIPET_LENGTH])


visualize_piano_roll(tracks[0],prefix='/tracks/track_1')
visualize_piano_roll(tracks[1],prefix='/tracks/track_2')

noise = tf.random_normal([1,8],dtype=tf.float32)

leaky_relu = lambda net: tf.nn.leaky_relu(net, alpha=0.025)


def generator_fn(noise, weight_decay=2.5e-5, is_training=True):
    with framework.arg_scope(
            [layers.fully_connected, layers.conv2d_transpose],
            activation_fn=tf.nn.relu, normalizer_fn=layers.batch_norm,
            weights_regularizer=layers.l2_regularizer(weight_decay)), \
         framework.arg_scope([layers.batch_norm], is_training=is_training,
                             zero_debias_moving_mean=True):
        net = layers.fully_connected(noise, 256,activation_fn=leaky_relu)
        net = layers.fully_connected(net, 512,activation_fn=leaky_relu)
        net = layers.fully_connected(net, 512, activation_fn=leaky_relu)
        net = layers.fully_connected(net, SNNIPET_LENGTH * 128, normalizer_fn=None, activation_fn=leaky_relu)
        net = tf.reshape(net, [-1, SNNIPET_LENGTH,128])
        return net



def discriminator_fn(fragment, unused_conditioning, weight_decay=2.5e-7,
                     is_training=True):

       with framework.arg_scope(
           [layers.conv2d, layers.fully_connected],
           activation_fn=leaky_relu, normalizer_fn=None,
           weights_regularizer=layers.l2_regularizer(weight_decay),
           biases_regularizer=layers.l2_regularizer(weight_decay)):
           net = layers.conv2d(fragment, 1024, 8, stride=2)
           net = layers.flatten(net)
           net = layers.fully_connected(net, 256, normalizer_fn=layers.batch_norm)
           with framework.arg_scope([layers.batch_norm], is_training=is_training):
               net = layers.fully_connected(net, 128, normalizer_fn=layers.batch_norm)
           return layers.linear(net, 1)

real_data_normed =  tf.convert_to_tensor(tracks, dtype=tf.float32)
chunk_queue = tf.train.slice_input_producer([real_data_normed])


# Build the generator and discriminator.
gan_model = tfgan.gan_model(
    generator_fn=generator_fn,  # you define
    discriminator_fn=discriminator_fn,  # you define
    real_data=chunk_queue,
    generator_inputs=noise)

gan_loss = tfgan.gan_loss(
    gan_model,
    generator_loss_fn=tfgan.losses.wasserstein_generator_loss,
    discriminator_loss_fn=tfgan.losses.wasserstein_discriminator_loss,
    gradient_penalty_weight=1.0)

l1_loss = tf.norm(gan_model.real_data - gan_model.generated_data, ord=1)
gan_loss = tfgan.losses.combine_adversarial_loss(gan_loss, gan_model, l1_loss, weight_factor=FLAGS.weight_factor)

train_ops = tfgan.gan_train_ops(gan_model,gan_loss,generator_optimizer=tf.train.AdamOptimizer(learning_rate=0.01, beta1=0.9, beta2=0.999, epsilon=1e-7),discriminator_optimizer=tf.train.AdamOptimizer(learning_rate=0.01, beta1=0.9, beta2=0.999, epsilon=1e-7))
#train_ops.global_step_inc_op = tf.train.get_global_step().assign_add(1)


#store_output_and_check_loss(gan_loss, gan_model.generated_data, gan_model.real_data, num_of_samples=3, prefix='gen',logdir=log_folder)

global_step_tensor = tf.Variable(1, trainable=False, name='global_step')
global_step = tf.train.get_or_create_global_step()
train_step_fn = tfgan.get_sequential_train_steps( train_steps=tf.contrib.gan.GANTrainSteps(1, 3))
with monitored_session.MonitoredTrainingSession(checkpoint_dir=log_folder) as session:
    loss = None
    for x in xrange(0,500):
        cur_loss, _ = train_step_fn(session, train_ops, global_step, train_step_kwargs={})

        gen_loss_np = session.run(gan_loss.generator_loss)
        dis_loss_np = session.run(gan_loss.discriminator_loss)
        print('iteration:' + str(x))
        print('Generator loss: %f' % gen_loss_np)
        print('Discriminator loss: %f' % dis_loss_np)
        if x % 10 == 0:
            store_image(session,gan_model.generated_data,iteration=x)

        #if (x) % 500 == 0:
        #   store_output_and_check_loss(session,gan_model.generated_data,prefix='rolled_'+str(round(gen_loss_np))+ '_' + str(SNNIPET_LENGTH) + '_gen_',num_of_samples=10)


